package main

import (
	"fmt"
	"time"
)

func getMoneyPerMin(price, min float64) (float64) {
	return price * (60/min)
}

func getMoneyPerHour(price float64, timeSoFar time.Duration) (float64) {
	return price/timeSoFar.Hours()
}

func printMoneyPerMin(price, min float64){
	moneyPerMin := getMoneyPerMin(price, min)
	fmt.Printf("If you finish in %d minutes, you'll make: $%0.2f/hr\n> ", int(min), moneyPerMin) }

func printMoneyPerHr(price float64, timeSoFar time.Duration) {
	moneyPerHour := getMoneyPerHour(price, timeSoFar)
	fmt.Printf("$%0.2f article written in %4.4vs is $%0.2f Dollars/hr\n> ", price, timeSoFar, moneyPerHour)
}

func startNewArticle(price float64) (float64) {
	fmt.Printf("> This article will make me $")
	_, b := fmt.Scanf("%f", &price)
	for (b!=nil) {
		_, b = fmt.Scanf("%f", &price)
		// TODO: Figure out how to print message without newlines in wrong places
		// newline after failed article will make message
		fmt.Printf("\n")
		fmt.Println("> Please enter a valid integer")
		fmt.Printf("> This article will make me $")
	}

	// not sure why this one has to be here
	fmt.Printf("> ")
	printMoneyPerMin(price, 15)
	printMoneyPerMin(price, 30)
	printMoneyPerMin(price, 45)
	return price
}

func waitForInput(price float64) {
	// set start time
	var startTime time.Time = time.Now()
	allMoneyPerHour := []float64{}
	allArticlePrices := []float64{}

	for {
		// read stdin
		var stdin string
		fmt.Scanf("%s", &stdin)

		switch string(stdin) {
		// force update
		case "":
			fmt.Printf("> ")
		case "u":
			timeSoFar := time.Since(startTime)
			printMoneyPerHr(price, timeSoFar)
		case "f":
			timeSoFar := time.Since(startTime)
			moneyPerHour := getMoneyPerHour(price, timeSoFar)

			fmt.Printf("---------------------------------------\nYou made $%0.2f/hr on this article by finishing in %v!\n---------------------------------------\n\n", moneyPerHour, timeSoFar)
			fmt.Printf("> Let's start a new article!\n")

			// append price to all prices
			allMoneyPerHour = append(allMoneyPerHour, moneyPerHour)

			// get average $/hr total
			var sumAllMoneyPerHour float64 = 0
			for i := range allMoneyPerHour {
				sumAllMoneyPerHour+=allMoneyPerHour[i]
			}

			// append price to all prices
			allArticlePrices = append(allArticlePrices, price)

			// sum of article prices
			var sumAllArticlePrices float64 = 0
			for i := range allArticlePrices {
				// can't use shorthand here, because I need a floating point number. Shorthand seems to only give back an int
				sumAllArticlePrices = sumAllArticlePrices + allArticlePrices[i]
			}

			fmt.Printf("\n>>>> Total made: $%0.2f, $%0.2f an hour with %d articles finished <<<<\n\n", sumAllArticlePrices, sumAllMoneyPerHour, len(allMoneyPerHour))

			price = startNewArticle(price)
			// got price for new article, reset time start
			startTime = time.Now()

			//fmt.Printf("> You've written %d articles!\n> ", len(allMoneyPerHour))
		case "h":
			help := `		
			commands
			==========
			h = help
			u = update
			f = finish`
			fmt.Printf("%s\n> ", help)
		default:
			fmt.Printf("'%s' is not a recognized command\n> ", stdin)
		}
	}
}

func startArticleTimer() {
	var price float64 = 3.02 // total dollars earned
	price = startNewArticle(price)

	// start the timer!
	waitForInput(price)

}

func main() {
	// start the article timer
	startArticleTimer()
}
